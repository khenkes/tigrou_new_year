const int MAX_BRIGHT = 255;
int fade_speed = 2;
const int LED_PIN = 10; 

void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_PIN, OUTPUT);
  }

// the loop function runs over and over again forever
void loop() {
  for (int fadeValue = 0 ; fadeValue <= MAX_BRIGHT; fadeValue += fade_speed) {
    // sets the value (range from 0 to 255):
    analogWrite(LED_PIN, fadeValue);
    // wait for 30 milliseconds to see the dimming effect
    delay(40);
  }

  // fade out from max to min in increments of 5 points:
  for (int fadeValue = MAX_BRIGHT ; fadeValue >= 0; fadeValue -= fade_speed) {
    // sets the value (range from 0 to 255):
    analogWrite(LED_PIN, fadeValue);
    // wait for 30 milliseconds to see the dimming effect
    delay(40);
  }

  switch (fade_speed) {
    case 3:
      fade_speed = 4;
    case 4:
      fade_speed = 2;
    case 2:
    fade_speed = 3;
  }
}
