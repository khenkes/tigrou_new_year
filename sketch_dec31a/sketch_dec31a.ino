const int RECV_PIN = 8;
const int LED_PIN = 3;
const int MAX_BRIGHT = 154;
const int MIN_BRIGHT = 3;
const int NO_BRIGHT = 0;
const int FADE_SPEED = 4;

void setup() {
  pinMode(RECV_PIN, INPUT);
  pinMode(LED_PIN, OUTPUT);

  Serial.begin(9600);
}
void loop() {
  int sensorValue = digitalRead(RECV_PIN);

  Serial.print(sensorValue + '\n');

  if (sensorValue == 1) {
    led_effect();
  } else {
    digitalWrite(LED_PIN, LOW);
  }

  delay(50);
}

void led_effect() {
  for (int fadeValue = NO_BRIGHT; fadeValue < MAX_BRIGHT; fadeValue += FADE_SPEED) {
    analogWrite(LED_PIN, fadeValue);    
    delay(40);
  }

  for (int fadeValue = MAX_BRIGHT; fadeValue > MIN_BRIGHT; fadeValue -= (FADE_SPEED + 1)) {
    analogWrite(LED_PIN, fadeValue);    
    delay(40);
  }

  for (int fadeValue = MIN_BRIGHT; fadeValue < MAX_BRIGHT; fadeValue += (FADE_SPEED + 2)) {
    analogWrite(LED_PIN, fadeValue);    
    delay(40);
  }
  
  for (int fadeValue = MAX_BRIGHT; fadeValue > MIN_BRIGHT; fadeValue -= (FADE_SPEED - 1)) {
    analogWrite(LED_PIN, fadeValue);    
    delay(40);
  }
  
  analogWrite(LED_PIN, MIN_BRIGHT);
  delay(3000);
  analogWrite(LED_PIN, NO_BRIGHT);
  delay(5000);
}

